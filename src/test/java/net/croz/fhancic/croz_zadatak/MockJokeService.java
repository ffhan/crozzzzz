package net.croz.fhancic.croz_zadatak;

import net.croz.fhancic.croz_zadatak.model.Category;
import net.croz.fhancic.croz_zadatak.model.Joke;
import net.croz.fhancic.croz_zadatak.service.JokeService;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

public class MockJokeService implements JokeService {
    @Override
    public List<Joke> getAllSorted() {
        return new LinkedList<>();
    }

    @Override
    public List<Joke> getAllSorted(Pageable pageable) {
        return null;
    }

    @Override
    public List<Joke> getAllByCategory(Category category) {
        return null;
    }

    @Override
    public void save(Joke joke) {

    }
}
