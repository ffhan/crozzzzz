package net.croz.fhancic.croz_zadatak.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "JOKES")
@Data
public class Joke implements Comparable<Joke> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String content;
    @Column(nullable = false)
    private Integer likes;
    @Column(nullable = false)
    private Integer dislikes;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    public Joke(String content, Integer likes, Integer dislikes, Category category) {
        this.content = content;
        this.likes = likes;
        this.dislikes = dislikes;
        this.category = category;
    }

    public Joke() {
    }

    @Override
    public int compareTo(Joke o) {
        return getLikeDislikeDifference() - o.getLikeDislikeDifference();
    }

    public Integer getLikeDislikeDifference() {
        return likes - dislikes;
    }
}
