package net.croz.fhancic.croz_zadatak.service;

import net.croz.fhancic.croz_zadatak.model.Category;
import net.croz.fhancic.croz_zadatak.model.Joke;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JokeService {
    List<Joke> getAllSorted();
    List<Joke> getAllSorted(Pageable pageable);

    List<Joke> getAllByCategory(Category category);

    void save(Joke joke);
}
