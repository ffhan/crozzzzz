package net.croz.fhancic.croz_zadatak.service.impl;

import net.croz.fhancic.croz_zadatak.model.Category;
import net.croz.fhancic.croz_zadatak.model.Joke;
import net.croz.fhancic.croz_zadatak.repository.JokeRepository;
import net.croz.fhancic.croz_zadatak.service.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JokeServiceImpl implements JokeService {
    private JokeRepository jokeRepository;

    @Autowired
    public JokeServiceImpl(JokeRepository jokeRepository) {
        this.jokeRepository = jokeRepository;
    }

    @Override
    public List<Joke> getAllSorted() {
        List<Joke> jokes = jokeRepository.findAll();
        jokes.sort(Joke::compareTo);
        return jokes;
    }

    @Override
    public List<Joke> getAllSorted(Pageable pageable) {
        List<Joke> jokes = jokeRepository.findAll(pageable).getContent();
        jokes.sort(Joke::compareTo);
        return jokes;
    }

    @Override
    public List<Joke> getAllByCategory(Category category) {
        return jokeRepository.getByCategory(category);
    }

    @Override
    public void save(Joke joke) {
        jokeRepository.save(joke);
    }
}
