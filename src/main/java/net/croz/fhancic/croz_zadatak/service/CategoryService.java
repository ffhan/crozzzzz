package net.croz.fhancic.croz_zadatak.service;

import net.croz.fhancic.croz_zadatak.model.Category;

public interface CategoryService {
    Category getById(Integer id);
    Category getByName(String name);
}
