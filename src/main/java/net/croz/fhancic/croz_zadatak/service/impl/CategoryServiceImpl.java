package net.croz.fhancic.croz_zadatak.service.impl;

import net.croz.fhancic.croz_zadatak.model.Category;
import net.croz.fhancic.croz_zadatak.repository.CategoryRepository;
import net.croz.fhancic.croz_zadatak.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category getById(Integer id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.getByName(name);
    }
}
