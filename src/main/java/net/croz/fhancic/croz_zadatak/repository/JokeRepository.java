package net.croz.fhancic.croz_zadatak.repository;

import net.croz.fhancic.croz_zadatak.model.Category;
import net.croz.fhancic.croz_zadatak.model.Joke;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JokeRepository extends JpaRepository<Joke, Long> {
    List<Joke> getByCategory(Category category);
    List<Joke> getByCategoryAndContentOrderById(Category category, String content);
}
