package net.croz.fhancic.croz_zadatak.repository;

import net.croz.fhancic.croz_zadatak.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Category getById(Integer id);
    Category getByName(String name);
}
