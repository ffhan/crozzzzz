package net.croz.fhancic.croz_zadatak.config;

import net.croz.fhancic.croz_zadatak.service.JokeService;
import net.croz.fhancic.croz_zadatak.service.impl.JokeServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/new").authenticated()
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetails user =
                    User.withDefaultPasswordEncoder()
                        .username("pperic")
                        .password("pero")
                        .roles("USER")
                        .build();
        UserDetails user2 =
                    User.withDefaultPasswordEncoder()
                        .username("iivic")
                        .password("ivan")
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(user, user2   );
    }
}
