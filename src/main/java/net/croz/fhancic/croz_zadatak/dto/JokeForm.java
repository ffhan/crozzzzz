package net.croz.fhancic.croz_zadatak.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JokeForm {
    private Integer categoryId;
    private String content;
}
