package net.croz.fhancic.croz_zadatak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrozZadatakApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrozZadatakApplication.class, args);
    }

}
