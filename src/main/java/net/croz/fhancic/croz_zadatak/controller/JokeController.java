package net.croz.fhancic.croz_zadatak.controller;

import net.croz.fhancic.croz_zadatak.dto.JokeForm;
import net.croz.fhancic.croz_zadatak.model.Category;
import net.croz.fhancic.croz_zadatak.model.Joke;
import net.croz.fhancic.croz_zadatak.service.CategoryService;
import net.croz.fhancic.croz_zadatak.service.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/joke")
public class JokeController {
    private JokeService jokeService;
    private CategoryService categoryService;

    @Autowired
    public JokeController(JokeService jokeService, CategoryService categoryService) {
        this.jokeService = jokeService;
        this.categoryService = categoryService;
    }

    @GetMapping("/new")
    public String addNewJoke() {
        return "new_joke.html";
    }

    @PostMapping("/new")
    public String addNewJoke(@RequestBody JokeForm jokeForm) {
        Category category = categoryService.getById(jokeForm.getCategoryId());
        Joke joke = new Joke(jokeForm.getContent(), 0, 0, category);
        jokeService.save(joke);
        return "redirect:index.html";
    }
}
