package net.croz.fhancic.croz_zadatak.controller;

import net.croz.fhancic.croz_zadatak.model.Category;
import net.croz.fhancic.croz_zadatak.model.Joke;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {
    @GetMapping("/")
    public String index() {
        return "index.html";
    }

    @GetMapping("/test")
    @ResponseBody
    public String testRestMetoda(@RequestParam(required = false) String ime) {

        if (ime != null) return "Pozdravljam " + ime;
        return "Pozdrav debilu";
    }

    @PostMapping("/test")
    @ResponseBody
    public Joke testRestMetoda2() {
        return new Joke("moj život", 4, 2, new Category(3, "test"));
    }
}
